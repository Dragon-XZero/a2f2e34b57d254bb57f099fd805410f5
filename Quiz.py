#Alfredo Guerra 13-9603-783

import requests
import time
import json

# Primera Pregunta, cuantos personajes hay en la primera pelicula?
resp = requests.get ("https://swapi.co/api/films/1/")
data = resp.json ()
personajes = data ['characters']
print("Star Wars the New Hope tiene " + str(len(personajes)) + " personajes")

# Segunda pregunta, cual pelicula tiene el opening crawl mas extenso?
mayor = 0
result = 0
for x in range (7):
    resp2 = requests.get("https://swapi.co/api/films/" + str(x+1) + "/")
    data2 = resp2.json()
    oc = data2['opening_crawl']

    for i in range (7):
        num = len(oc)
        if num > mayor:
            mayor = num
            result = x + 1

print("La Pelicula " + str(result) + " tiene el opening crawl mas largo con " + str(mayor) + " letras")